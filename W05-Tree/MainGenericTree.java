package main;

import java.util.Scanner;

public class MainGenericTree {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        GenericTree<String> elizabeth = new GenericTree<String>("ELIZABETH II");
        GenericTree<String> charles = new GenericTree<String>("CHARLES");
        GenericTree<String> anne = new GenericTree<String>("ANNE");
        GenericTree<String> andrew = new GenericTree<String>("ANDREW");
        GenericTree<String> edward = new GenericTree<String>("EDWARD");
        GenericTree william = new GenericTree("WILLIAM");
        GenericTree harry = new GenericTree("HARRY");
        GenericTree peter = new GenericTree("PETER");
        GenericTree zara = new GenericTree("ZARA");
        GenericTree beatrice = new GenericTree("BEATRICE");
        GenericTree eugenie = new GenericTree("EUGENIE");
        GenericTree louise = new GenericTree("LOUISE");
        GenericTree james = new GenericTree("JAMES");
        GenericTree george = new GenericTree("GEORGE");
        GenericTree charlotte = new GenericTree("CHARLOTTE");
        GenericTree louis = new GenericTree("LOUIS");
        GenericTree archie = new GenericTree("ARCHIE");
        GenericTree savannah = new GenericTree("SAVANNAH");
        GenericTree isla = new GenericTree("ISLA");
        GenericTree mia = new GenericTree("MIA");
        GenericTree lena = new GenericTree("LENA");

        elizabeth.addChildren(charles);
        elizabeth.addChildren(anne);
        elizabeth.addChildren(andrew);
        elizabeth.addChildren(edward);

        charles.addChildren(william);
        charles.addChildren(harry);

        anne.addChildren(peter);
        anne.addChildren(zara);

        andrew.addChildren(beatrice);
        andrew.addChildren(eugenie);

        edward.addChildren(louise);
        edward.addChildren(james);

        william.addChildren(george);
        william.addChildren(charlotte);
        william.addChildren(louis);

        harry.addChildren(archie);

        peter.addChildren(savannah);
        peter.addChildren(isla);

        zara.addChildren(mia);
        zara.addChildren(lena);

        /* Menampilkan Silsilah Keluarga ELIZABETH II */
        elizabeth.printPathFromRoot(0);
        System.out.print("\n");

        /*
         * Menampilkan Hasil Pencarian Nama Orang yang Ada pada Pohon Keluarga Bangsawan
         */
        System.out.print("Siapa yang Anda cari? ");
        elizabeth.search(input.nextLine());

        input.close();
    }
}
