package main;

class BinaryGenericTree<T> {
    private static int level;
    private T value;
    private BinaryGenericTree left;
    private BinaryGenericTree right;

    public BinaryGenericTree(T value) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public BinaryGenericTree(T value, BinaryIntegerTree left, BinaryIntegerTree right) {
        this.value = value;
        this.left = null;
        this.right = null;
    }

    public void setRight(BinaryGenericTree right) {
        this.right = right;
    }

    public void setLeft(BinaryGenericTree left) {
        this.left = left;
    }

    public BinaryGenericTree<T> getRight() {
        return right;
    }

    public BinaryGenericTree<T> getLeft() {
        return left;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public void print(int level) {
        System.out.println(value);
        level++;

        if (left != null) {
            System.out.print("  ".repeat(level));
            left.print(level);
        }
        if (right != null) {
            System.out.print("  ".repeat(level));
            right.print(level);
        }
    }
}