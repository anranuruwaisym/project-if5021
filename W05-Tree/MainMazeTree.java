package main;

public class MainMazeTree {
    public static void main(String[] args) {
        BinaryIntegerTree node1 = new BinaryIntegerTree(1);
        BinaryIntegerTree node5 = new BinaryIntegerTree(5);
        BinaryIntegerTree node6 = new BinaryIntegerTree(6);
        BinaryIntegerTree node9 = new BinaryIntegerTree(9);
        BinaryIntegerTree node7 = new BinaryIntegerTree(7);
        BinaryIntegerTree node8 = new BinaryIntegerTree(8);
        BinaryIntegerTree node4 = new BinaryIntegerTree(4);
        BinaryIntegerTree node13 = new BinaryIntegerTree(13);
        BinaryIntegerTree node11 = new BinaryIntegerTree(11);
        BinaryIntegerTree node15 = new BinaryIntegerTree(15);
        BinaryIntegerTree node16 = new BinaryIntegerTree(16);

        node1.setLeft(node5);
        node5.setLeft(node6);
        node5.setRight(node9);
        node6.setLeft(node7);
        node9.setLeft(node13);
        node7.setLeft(node8);
        node7.setRight(node11);
        node8.setLeft(node4);
        node11.setLeft(node15);
        node15.setLeft(node16);

        node1.finish(node1, 16);
    }
}
