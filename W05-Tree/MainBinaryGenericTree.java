package main;

public class MainBinaryGenericTree {
    public static void main(String[] args) {
        /* Menggunakan data nama */
        BinaryGenericTree<String> node1 = new BinaryGenericTree<String>("Anranur UM");

        /* Menggunakan data tahun lahir dan tahun saat ini */
        BinaryGenericTree<Integer> node2 = new BinaryGenericTree<Integer>(1996);
        BinaryGenericTree<Integer> node3 = new BinaryGenericTree<Integer>(2021);

        /* Menggunakan data tinggi badan dan berat badan waktu lahir */
        BinaryGenericTree<Double> node4 = new BinaryGenericTree<Double>(52.00);
        BinaryGenericTree<Double> node5 = new BinaryGenericTree<Double>(4.4);

        /* Menggunakan data tinggi badan dan berat badan saat ini */
        BinaryGenericTree<Double> node6 = new BinaryGenericTree<Double>(167.5);
        BinaryGenericTree<Double> node7 = new BinaryGenericTree<Double>(64.5);

        node1.setLeft(node2);
        node1.setRight(node3);

        node2.setLeft(node4);
        node2.setRight(node5);

        node5.setLeft(node6);
        node5.setRight(node7);

        node1.print(0);
    }
}
