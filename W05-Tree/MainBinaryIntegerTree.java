package main;

public class MainBinaryIntegerTree {
    public static void main(String[] args) {

        BinaryIntegerTree node1 = new BinaryIntegerTree(2);
        BinaryIntegerTree node2 = new BinaryIntegerTree(7);
        BinaryIntegerTree node3 = new BinaryIntegerTree(5);
        BinaryIntegerTree node4 = new BinaryIntegerTree(2);
        BinaryIntegerTree node5 = new BinaryIntegerTree(6);
        BinaryIntegerTree node6 = new BinaryIntegerTree(5);
        BinaryIntegerTree node7 = new BinaryIntegerTree(11);
        BinaryIntegerTree node8 = new BinaryIntegerTree(9);
        BinaryIntegerTree node9 = new BinaryIntegerTree(4);

        node1.setLeft(node2);
        node1.setRight(node3);
        node2.setLeft(node4);
        node2.setRight(node5);
        node5.setLeft(node6);
        node5.setRight(node7);
        node3.setLeft(null);
        node3.setRight(node8);
        node8.setLeft(node9);
        node8.setRight(null);

        node1.print(0);
    }
}
