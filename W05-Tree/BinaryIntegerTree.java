package main;

import java.util.ArrayList;

public class BinaryIntegerTree {
    public static int level;
    private int value;
    private BinaryIntegerTree left;
    private BinaryIntegerTree right;

    public BinaryIntegerTree(int value) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public BinaryIntegerTree(int value, BinaryIntegerTree left, BinaryIntegerTree right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public int getValue() {
        return value;
    }

    public BinaryIntegerTree getLeft() {
        return left;
    }

    public BinaryIntegerTree getRight() {
        return right;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setLeft(BinaryIntegerTree left) {
        this.left = left;
    }

    public void setRight(BinaryIntegerTree right) {
        this.right = right;
    }

    public void print(int level) {
        System.out.println(value);

        level++;

        if (left != null) {
            System.out.print("  ".repeat(level));
            left.print(level);
        }
        if (right != null) {
            System.out.print("  ".repeat(level));
            right.print(level);
        }
    }

    private Boolean hasPath(BinaryIntegerTree root, ArrayList<Integer> arr, int x) {
        if (root == null)
            return false;

        arr.add(root.getValue());

        if (root.getValue() == x)
            return true;

        if (hasPath(root.getLeft(), arr, x) || hasPath(root.getRight(), arr, x))
            return true;

        /*
         * required node does not lie either in the left or right subtree of the current
         * node And then, remove current node's value from 'arr'and then return false
         */
        arr.remove(arr.size() - 1);
        return false;
    }

    public void finish(BinaryIntegerTree root, int x) {
        // ArrayList to store the path
        ArrayList<Integer> arr = new ArrayList<>();

        // if required node 'x' is present then print the path
        if (hasPath(root, arr, x)) {
            for (int i = 0; i < arr.size() - 1; i++)
                System.out.print(arr.get(i) + " -> ");

            System.out.print(arr.get(arr.size() - 1));
        }

        // 'x' is not present in the binary tree
        else
            System.out.print("Tidak Ada Path");
    }
}
