package main;

import java.util.ArrayList;
import java.util.Locale;

class GenericTree<T> {
    public T value;
    public ArrayList<GenericTree<T>> children;

    public GenericTree(T value) {
        this.value = value;
        this.children = new ArrayList<GenericTree<T>>();
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public void addChildren(GenericTree<T> children) {
        this.children.add(children);
    }

    public ArrayList<GenericTree<T>> getChildren() {
        return this.children;
    }

    public void printPathFromRoot(int level) {
        for (int i = 0; i < level; ++i) {
            System.out.print("   ");
        }

        System.out.println(value);

        for (GenericTree child : getChildren()) {
            child.printPathFromRoot(level + 1);
        }
    }

    private int getNumberOfElements(GenericTree<T> tree) {
        int n = 1;
        if (!isTreeEmpty(tree)) {
            if (tree.getChildren() != null) {
                for (GenericTree<T> child : tree.getChildren()) {
                    n += getNumberOfElements(child);
                }
            }
        }
        return n;
    }

    private int getNumberOfLeaves(GenericTree<T> tree) {
        int n = 0;
        if (isLeaf(tree)) {
            n = 0;
        } else {
            for (GenericTree<T> child : tree.getChildren()) {
                n += getNumberOfLeaves(child);
            }
        }

        return n;
    }

    public int getHeight(GenericTree<T> tree) {
        int n = 0;
        if (isLeaf(tree)) {
            n = 1;
        } else {
            for (GenericTree<T> child : tree.getChildren()) {
                if (getHeight(child) > n)
                    n = 1 + getHeight(child);
            }
        }
        return n;
    }

    private boolean isTreeEmpty(GenericTree<T> tree) {
        return getNumberOfLeaves(tree) == 0;
    }

    private boolean isLeaf(GenericTree<T> tree) {
        return tree.getChildren().isEmpty();
    }

    public void search(T value) {
        Integer elements, leaves, height;

        if (getChildren().isEmpty()) {
            if (getValue().toString().toLowerCase().contains(value.toString().toLowerCase()))
                printPathFromRoot(0);
        } else {
            for (GenericTree<T> child : getChildren()) {
                if (child.getValue().toString().toLowerCase().contains(value.toString().toLowerCase())) {
                    child.printPathFromRoot(0);

                    elements = getNumberOfElements(child);
                    leaves = getNumberOfLeaves(child);
                    height = getHeight(child);

                    System.out.println("Total Elemen: " + elements + "\n" + "Total Daun: " + leaves + "\n"
                            + "Ketinggian: " + height);
                    return;
                } else {
                    child.search(value);
                }
            }
        }
    }
}
